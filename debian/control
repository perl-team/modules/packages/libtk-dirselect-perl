Source: libtk-dirselect-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Dominique Dumont <dod@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: perl,
                     perl-tk,
                     libtest-pod-perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtk-dirselect-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtk-dirselect-perl.git
Homepage: https://metacpan.org/release/Tk-DirSelect

Package: libtk-dirselect-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         perl-tk
Description: cross-platform directory selection widget
 Tk::DirSelect provides a cross-platform directory selection widget. A
 context menu (right-click or <Button3>) allows the
 creation, renaming, and deletion of directories while browsing.
 .
 Note: Perl/Tk 804 added the chooseDirectory method which uses native
 system dialogs where available. Unfortunately, a non-existent
 directory cannot (yet?) be chosen. If you want a native feel for
 your program, and do not need non-existent directory choice, you
 probably want to use chooseDirectory method instead of Tk::DirSelect.
